FROM ruby:2.5.5-alpine

ARG PRECOMPILEASSETS
ARG RAILS_ENV

ENV SECRET_KEY_BASE foo
ENV RAILS_ENV ${RAILS_ENV}
ENV RAILS_SERVE_STATIC_FILES true

RUN apk add --update --no-cache \
    build-base \
    git \
    postgresql-dev \
    postgresql-client \
    imagemagick \
    nodejs-current \
    yarn \
    python2 \
    tzdata \
    file

RUN gem install bundler
# Install gems
RUN mkdir /gems
WORKDIR /gems
COPY Gemfile .
COPY Gemfile.lock .
RUN bundle install -j4 --retry 3 \
    # Remove unneeded files (cached *.gem, *.o, *.c)
    && rm -rf /usr/local/bundle/cache/*.gem \
    && find /usr/local/bundle/gems/ -name "*.c" -delete \
    && find /usr/local/bundle/gems/ -name "*.o" -delete

ARG INSTALL_PATH=/railsondocker
ENV INSTALL_PATH $INSTALL_PATH
WORKDIR $INSTALL_PATH
COPY . .

# Precompile assets (or not)
RUN docker/potential_asset_precompile.sh $PRECOMPILEASSETS

CMD ["docker/startup.sh"]
