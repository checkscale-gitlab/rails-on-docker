#! /bin/sh

./docker/wait_for_services.sh
./docker/prepare_db.sh
bundle exec puma -C config/puma.rb
