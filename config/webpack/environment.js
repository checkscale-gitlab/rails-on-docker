const { environment } = require('@rails/webpacker')
const webpack = require('webpack')

const StyleLintPlugin = require('stylelint-webpack-plugin')

environment.plugins.prepend(
  'StyleLint',
  new StyleLintPlugin({
    configFile: 'stylelint.config.js',
    files: ['app/webpacker/sass/*.scss', 'app/webpacker/sass/*/*.scss'],
    syntax: 'scss'
  })
)

module.exports = environment
